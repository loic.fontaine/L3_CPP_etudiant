#ifndef LISTE_CPP
#define LISTE_CPP

#include <iostream>
#include "liste.hpp"

using namespace std;


liste liste::Liste(){
    this->tete=nullptr;
}

void liste::ajouerDevant(int valeur){
    //si la liste est vide, alors on ajoute la valeur
    Noeud*nouveau= new Noeud;
    nouveau->valeur=valeur;
    nouveau->suivant=tete;
    tete=nouveau;
}

int const liste::getTaille(){
    Noeud*courant=tete;
    int taille=0;
    while(courant){
        taille++;
        courant=courant->suivant;
    }
    return taille;
}

int const liste::getElement(int indice){
    Noeud*courant=tete;
    int cpt=0;
    while(courant){
        cpt++;
        if(cpt==indice){
            return courant->valeur;
        }
        courant=courant->suivant;
    }
}

void liste::afficher(){
    Noeud*courant=tete;
    while(courant){
        cout<<"  "<<courant->valeur<<" ";
        courant=courant->suivant;
    }
    cout<<"\n";
}

#endif //liste.cpp
