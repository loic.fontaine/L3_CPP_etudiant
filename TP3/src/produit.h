#ifndef PRODUIT_H
#define PRODUIT_H
#include <iostream>
#include <string>


using namespace std;
class Produit
{
public:
    Produit(int id, const string& description);
    int getId()const;
    const string& getDescription()const;
    void afficherProduit()const;
private:
    int _id;
    string _description;
};

#endif // PRODUIT_H
