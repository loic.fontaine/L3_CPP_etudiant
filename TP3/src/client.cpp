#include "client.h"
#include <iostream>

using namespace std;
Client::Client(int id,const std::string& nom)
{
    _id=id;
    _nom=nom;
}

int Client::getId()const{ return _id;}
const string& Client::getNom()const{return _nom;}
void Client::afficherClient()const{cout<<"Client("<<getId()<<","<<getNom()<<")"<<endl;}
