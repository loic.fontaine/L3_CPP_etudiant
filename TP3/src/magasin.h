#ifndef MAGASIN_H
#define MAGASIN_H
#include <vector>
#include "client.h"
#include "produit.h"
#include "location.h"
using namespace std;

class Magasin
{
public:
    Magasin();
    int nbClients()const;
    void ajouterClient(const string& nom);
    void afficherClients()const;
    void supprimerClient(int _idClient);
private:
    vector<Client>_clients;
    vector<Produit>_produits;
    vector<Location>_locations;
    int _idCourantClient;
    int _idCourantProduit;
};

#endif // MAGASIN_H
