#include "magasin.h"
#include "location.h"
#include "client.h"
#include "produit.h"
#include <iostream>

using namespace std;

int main() {
    Magasin m;
    m.ajouterClient("toto");
    m.ajouterClient("tata");
    m.afficherClients();
    m.supprimerClient(2);
    m.afficherClients();
    return 0;
}

