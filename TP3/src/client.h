#ifndef CLIENT_H
#define CLIENT_H
#include <string>
using namespace std;

class Client
{
public:
    Client(int id,const string & nom);
    int getId()const;
    const string& getNom()const;
    void afficherClient()const;
private:
    int _id;
    string _nom;
};

#endif // CLIENT_H
