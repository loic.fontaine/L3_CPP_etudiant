#include <iostream>
#include "magasin.h"

using namespace std;


Magasin::Magasin()
{
    _idCourantClient=0;
    _idCourantProduit=0;
}

int Magasin::nbClients()const{
    return _clients.size();
}

void Magasin::ajouterClient(const string &nom){
      _idCourantClient++;
    Client c(_idCourantClient,nom);
    _clients.push_back(c);
}

void Magasin::afficherClients()const{
    for(int i=0;i<_clients.size();i++){
        _clients[i]. afficherClient();
    }
}

void Magasin::supprimerClient(int _idClient){
    for(int i=0;i<_clients.size();i++){
        if(_clients[i].getId()==_idClient){
            swap(_clients[_clients.size()-1],_clients[i]);
            _clients.pop_back();
        }
    }
}
